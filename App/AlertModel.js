import React, {useEffect} from 'react';
import {
  Image,
  Modal,
  Text,
  View,
  TouchableOpacity,
  StyleSheet,
  TextInput,
  KeyboardAvoidingView,
  Platform,
} from 'react-native';
import ButtonComponent from './Component/ButtonComponent';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalOuter: {
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    position: 'absolute',
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  card: {
    width: '85%',
    borderRadius: 15,
    overflow: 'hidden',
  },
  closeBox: {
    position: 'absolute',
    left: 7,
    top: 15,
    paddingHorizontal: 5,
    width: 40,
    height: 40,
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 20,
  },
  footer: {
    marginTop: 40,
    textAlign: 'center',
  },
  title: {
    textAlign: 'center',
    width: '80%',
    alignSelf: 'center',
  },
  subTitle: {
    textAlign: 'center',
    lineHeight: 20,
    margin: 10,
  },
  bottomBtn: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginTop: 30,
    marginHorizontal: 20,
  },
  btnOuter: {
    paddingHorizontal: 15,
    borderRadius: 5,
    marginBottom: 35,
    marginTop: 20,
    minHeight: 50,
  },
  myclose: {
    width: 20,
    height: 20,
    tintColor: '#1E0A3A',
  },
  headerTitle: {
    color: '#000',
    fontSize: 22,
  },
  headerImage1: {
    width: 64,
    height: 64,
  },
  cardTitle: {
    fontSize: 22,
    color: '#000',
    textAlign: 'center',
  },
  myTextinput:{
    padding: 10,
    textAlignVertical: 'top',
  }
});

const AlertModal = ({
  visible,
  onClose,
  leftBtn,
  rightBtn,
  data,
  closebtn,
  subtitle,
  headerTextStyle,
  cardColor,
  headerImage1,
  cardMargin,
  cardtitile,
  MytextInput,
  cardAnim = 'none',
  isProcessing = false,
}) => {
  if (!data) {
    return null;
  }

  const renderModal = () => {
    const {header, title, subTitle, textInput, leftBtnName, rightBtnName} = data;
    return (
      <KeyboardAvoidingView
        style={styles.container}
        behavior={Platform.OS === 'ios' ? 'padding' : 'height'}>
          <TouchableOpacity style={styles.modalOuter} onPress={onClose}>
            <View />
          </TouchableOpacity>
          <View style={[styles.card, {backgroundColor: cardColor || '#fff'}]}>
            <View style={styles.header}>
              {(header?.btnClose && (
                <TouchableOpacity style={styles.closeBox} onPress={onClose}>
                  <Image
                    resizeMode={'contain'}
                    source={header?.closeimage}
                    style={closebtn?.style || styles.myclose}
                  />
                </TouchableOpacity>
              )) ||
                null}
              <View>
                <Image
                  resizeMode={'contain'}
                  source={header?.headerImage1}
                  style={headerImage1?.style || styles.headerImage1}
                />
                <Text style={headerTextStyle?.style || styles.headerTitle}>
                  {header?.title}
                </Text>
              </View>
            </View>
            <View
              style={{
                marginHorizontal: 20,
              }}>
              <Text
                style={
                  [cardtitile?.style, {textAlign: 'center'}] || styles.cardTitle
                }>
                {title?.text}
              </Text>
            </View>
            {textInput &&
              ((
                <View
                  style={{marginHorizontal: cardMargin || 25, marginTop: 10}}>
                  <TextInput
                    placeholder={MytextInput?.style?.placeholder || 'Hello'}
                    multiline={true}
                    numberOfLines={6}
                    underlineColorAndroid="transparent"
                    onChangeText={text => MytextInput?.onChangeText(text)}
                    style={[
                  styles.myTextinput,
                     { height: MytextInput?.style?.height || 120,
                      borderWidth: MytextInput?.style?.borderWidth || 1 / 2,
                      borderRadius: MytextInput?.style?.borderRadius || 5,
                      borderColor: MytextInput?.style?.borderColor || '#000',}
                    ]}
                  />
                </View>
              ) ||
                null)}
            <View style={styles.footer}>
              {(subTitle && (
                <Text style={[styles.subTitle, subtitle?.style]}>
                  {subTitle?.text}
                </Text>
              )) ||
                null}
              <View style={styles.bottomBtn}>
                {(leftBtnName && (
                  <ButtonComponent
                    isProcessing={isProcessing}
                    onPress={leftBtn.onPress}
                    title={leftBtn.title}
                    {...leftBtn.style}
                    style={styles.btnOuter}
                    txtStyle={leftBtn?.fontstyle}
                  />
                )) ||
                  null}
                {(rightBtnName && (
                  <ButtonComponent
                    onPress={rightBtn.onPress}
                    title={rightBtn.title}
                    {...rightBtn.style}
                    style={styles.btnOuter}
                    txtStyle={leftBtn?.fontstyle}
                  />
                )) ||
                  null}
              </View>
            </View>
          </View>
      </KeyboardAvoidingView>
    );
  };

  return (
    <Modal visible={visible} transparent={true} animationType={cardAnim}  onRequestClose={onClose}>
      {renderModal()}
    </Modal>
  );
};

export default AlertModal;
