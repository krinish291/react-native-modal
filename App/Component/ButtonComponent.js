import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ActivityIndicator,
  StyleSheet,
} from 'react-native';

const styles = StyleSheet.create({
  outer: {
    paddingHorizontal: 20,
    paddingVertical: 10,
    borderRadius: 6,
    borderWidth: 1,
    justifyContent: 'center',
    alignItems: 'center',
    // ...CommonStyle.center,
    marginVertical: 5,
    minWidth: 100,
  },
  btnOuter: {
    alignSelf: 'center',
  },
});

export default function ButtonComponent(props) {
  const {
    title,
    onPress,
    exStyle,
    style,
    border,
    backColor,
    textColor,
    isProcessing,
    txtStyle,
  } = props;

  const {outer, btnOuter} = styles;

  return (
    <TouchableOpacity
      style={[btnOuter, exStyle && exStyle]}
      onPress={onPress}
      disabled={isProcessing}>
      <View
        style={[
          outer,
          {
            backgroundColor: backColor || '#fff',
            borderColor: border || '#000',
          },
          style,
        ]}>
        {(!isProcessing && (
          <Text
            style={{
              color: textColor || '#0000',
              fontSize: txtStyle?.fontSize || 12,
            }}>
            {title}
          </Text>
        )) || <ActivityIndicator color={textColor || '#fff'} />}
      </View>
    </TouchableOpacity>
  );
}
