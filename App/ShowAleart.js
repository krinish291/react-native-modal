import React, {useState} from 'react';
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';
import AlertModal from './AlertModel';

const COLOR = '#cecece';

const alertData = {
  confirmLogout: {
    header: {
      title: 'Hello',
      btnClose: true,
      closeimage: require('./image/close.png'),
      headerImage1: require('./image/alert.png'),
    },
    title: {
      text: 'Are you sure you want to logout?',
    },
    textInput: true,
    leftBtnName: 'Yes',
    rightBtnName: 'No',
    // subTitle: {
    //   text: 'Hello',
    // },
  },
  deleteCard: {
    header: {
      title: '',
      btnClose: true,
    },
    title: {
      text: 'Are you sure you want to delete the card ending in ',
    },
    leftBtn: 'Delete',
    rightBtn: 'Cancel',
  },
};

const styles = StyleSheet.create({
  container: {flex: 1, alignItems: 'center', justifyContent: 'center'},
  alertBtn: {padding: 10, color: '#fff'},
});
export default function ShowAlert() {
  const [showAlert, setShowAlert] = useState(false);
  const [isProcessing, setIsProcessing] = useState(false);

  const {container, alertBtn} = styles;

  const manageAlert = (alert = false) => {
    setShowAlert(alert);
  };

  const alertDetails = alertData.confirmLogout;

  const leftBtn = () => {
    console.log('leftBtn');
  };

  const rightBtn = () => {
    console.log('RightBtn');
  };

  const onChangeText = e => {
    console.log(e);
  };

  return (
    <View style={container}>
      <TouchableOpacity
        style={{
          backgroundColor: '#FA6650',
          alignItems: 'center',
          marginHorizontal: 20,
          borderRadius: 3,
        }}
        onPress={() => manageAlert(true)}>
        <Text style={alertBtn}>ShowAlert</Text>
      </TouchableOpacity>
      <AlertModal
        visible={showAlert}
        onClose={manageAlert}
        data={alertDetails}
        isProcessing={isProcessing}
        cardMargin={25}
        cardAnim={'slide'}
        // cardColor={'red'}
        headerImage1={{
          style: {
            width: 64,
            height: 64,
          },
        }}
        headerTextStyle={{
          style: {
            fontSize: 25,
            color: 'gray',
            marginTop: 20,
          },
        }}
        MytextInput={{
          style: {
            // placeholder: 'Okok',
            borderColor: COLOR,
            borderRadius: 10,
            height: 120,
            borderWidth: 1,
          },
          onChangeText: onChangeText,
        }}
        cardtitile={{
          style: {
            fontSize: 22,
            color: '#000',
          },
        }}
        closebtn={{
          style: {
            width: 20,
            height: 20,
            tintColor: '#1E0A3A',
          },
        }}
        subtitle={{
          style: {
            color: COLOR,
            fontSize: 16,
          },
        }}
        leftBtn={{
          title: alertDetails?.leftBtnName,
          style: {
            textColor: '#000',
            backColor: '#fff',
            border: COLOR,
          },
          fontstyle: {
            fontSize: 13,
          },
          onPress: leftBtn,
        }}
        rightBtn={{
          title: alertDetails?.rightBtnName,
          style: {
            textColor: '#000',
            backColor: '#fff',
            border: COLOR,
          },
          onPress: rightBtn,
        }}
      />
    </View>
  );
}
